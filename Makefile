default: build/ramm build/rasm

build:
	mkdir -p build 
	
build/ramm: build src/ramm.c src/ramm.h	
	gcc -o build/ramm src/ramm.c

build/rasm: build src/rasm.py
	cp src/rasm.py build/rasm
