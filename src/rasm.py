#!/usr/bin/env python3
import argparse
import struct
from sys import stderr
RAMM_IS = {
                "HLT":0x00,
                "LDA":0x12,
                "IAD":0x14,
                "ISB":0x15,
                "LDQ":0x16,
                "STA":0x20,
                "STQ":0x21,
                "IMU":0x24,
                "IDV":0x25,
                "AZJ":0x36,
                "AMJ":0x37,
                "NOP":0x50,
                "RDI":0x65,
                "PRI":0x66,
                "UNJ":0x75,
                "END":0x99
              }
lookup = lambda x: RAMM_IS[x]

def tokenized(string):
    location = string[0:4].strip()
    operation = string[6:9].strip()
    operand = string[10:14].strip()
    offset = string[14:17].strip()
    if offset and not offset.isspace():
        offset = int(offset)

    else:
        offset = None

    return (location, operation, operand, offset)

def assemble(in_fname, out_fname):
    symbols = {}
    num_bytes = 0
    symbol_table = {}

    # First pass:
    #  - discover labels and constants
    #  - counts the number of bytes needed for program code.
    with open(in_fname, 'r') as asm:
        for line in asm:
            line_tokens = tokenized(line)

            if line_tokens[0] and not line_tokens[0].isspace():
                symbol_table[line_tokens[0]] = num_bytes

            if  line_tokens[1]  not in {'BSS', 'DEC'}:
                num_bytes += 2 # Count how many bytes the program needs
            else:
                symbols[line_tokens[0]] = (line_tokens[1], int(line_tokens[2]))

    # Linking step
    for symbol, location in zip(symbols.keys(), range(num_bytes, num_bytes + 2 * len(symbols), 2)):
        symbol_table[symbol] = location

    # Second pass:
    #   - Translate assembly to machine code
    #   - Output linked object file
    num_bytes = 0
    with open(in_fname, 'r') as asm:
        with open(out_fname, 'wb') as obj:
            for line in asm:
                line_tokens = tokenized(line)

                # Operand-Offset instructions
                if line_tokens[1] in {"AZJ", "AMJ", "UNJ"}:
                    offset = symbol_table[line_tokens[2]] +\
                        (2 * line_tokens[3] if line_tokens[3] is not None else 0)

                    num_bytes += obj.write(bytes([lookup(line_tokens[1])]) + struct.pack('>h', offset))
                    #obj.write(bytes([lookup(line_tokens[1]), offset]))

                # Memory location operand
                elif line_tokens[1] in {"LDA", "LDQ", "STA", "STQ", "RDI", "PRI", "IAD",
                                        "ISB", "IMU", "IDV"}:

                        num_bytes += obj.write(bytes([lookup(line_tokens[1])]) + struct.pack('>h', symbol_table[line_tokens[2]]))

                    #obj.write(bytes([lookup(line_tokens[1]), symbol_table[line_tokens[2]]]))

                # Ignore BSS and DEC. These are dealt with in different steps
                elif line_tokens[1] in {"BSS", "DEC"}:
                    pass

                # No-argument instructions
                else: # HLT, NOP, END

                    num_bytes += obj.write(bytes([0, lookup(line_tokens[1])]))
                    #obj.write(bytes([lookup(line_tokens[1]), 0])

            symb_mode = lambda key: symbols[key][0]
            symb_value = lambda key: symbols[key][1]
            symb_location = lambda key: symbol_table[key]

            data_segment = b''

            for symbol in sorted(symbols.keys(), key=symb_location):
                if symb_mode(symbol) == "DEC":
                    data_segment += struct.pack('>h', symb_value(symbol))
                elif symb_mode(symbol) == "BSS":
                    data_segment += struct.pack('>h', symb_value(symbol))
                else:
                    raise ValueError("BLagh!")

            num_bytes += obj.write(bytes(data_segment))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Compile RAMM assembly code into RAMM bytecode")
    parser.add_argument("in_fname", metavar="IN", type=str,
                        help="Assembly file to process")
    parser.add_argument("-o", "--output", dest="out_fname", metavar="OUT", default="a.out",
                        help="Output filename")

    args = parser.parse_args()

    assemble(args.in_fname, args.out_fname)

