#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#define HLT 0x00
#define LDA 0x12
#define IAD 0x14
#define ISB 0x15
#define LDQ 0x16
#define STA 0x20
#define STQ 0x21
#define IMU 0x24
#define IDV 0x25
#define AZJ 0x36
#define AMJ 0x37
#define NOP 0x50
#define RDI 0x65
#define PRI 0x66
#define UNJ 0x75
#define END 0x99
/*
#define op (byte)(*(current)%0x100)
#define yy (byte*)(machine.mem + *(current)/0x100)
*/
typedef short reg;
typedef unsigned char byte;
typedef unsigned short word;
typedef unsigned int dword;

typedef struct RAMM{
  byte* mem;
  byte* ip;
  reg A, Q;
} RAMM;

void dumpMem(int, RAMM);
RAMM initializeMachine(int, const char*);
