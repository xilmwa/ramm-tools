#include "ramm.h"
void dumpRegs(RAMM machine);

int 
main (const int argc, const char** argv) {

    if (argc < 2) {
        printf("usage: %s program_name\n", argv[0]);   
        return -1; 
    }
    // Create a new machine and get information about it
    const int     SZ=100; // Specification calls for 100 words of RAM.
    RAMM    machine  = initializeMachine(SZ,argv[1]);

    dword         mul_tmp = 0x00000000;  // Temporary dword for handling multiplication
    reg           reg_tmp = 0x0000;      // Temporary register for division
    char          str_tmp[6];            // Temporary string for RDI

    byte          op,yy;                    // Word -> byte conversions
    bool          debug = true;                 // Debug mode

    // Main Loop
    do {
        yy = *(machine.ip); // Get argument from low byte
        op = *(machine.ip + 1); // Get opcode from high byte
        //dumpRegs(machine);
        switch(op){
        case HLT:
            break;
        case LDA:                                               // LDA:
            machine.A = *(word*)(machine.mem + yy);                        // Load *(yy) into rA
            machine.ip += 2;                                          // Advance to next word
            break;;
        case IAD:                                               // IAD:
            machine.A += *(word*)(machine.mem + yy);                       // Add yy to rA
            machine.ip += 2;                                          // Advance to next word
            break;;
        case ISB:                                               // ISB:
            machine.A -= *(word*)(machine.mem + yy);                       // Sub yy from rA
            machine.ip += 2;                                          // Advance to next word
            break;;
        case LDQ:                                               // LDQ:
            machine.Q = *(word*)(machine.mem + yy);                        // Load yy into rQ
            machine.ip += 2;                                          // Advance to next word
            break;;
        case STA:                                               // STA:
            *(word*)(machine.mem + yy) = machine.A;                        // Store rA in yy
            machine.ip += 2;                                          // Advance to next word
            break;;
        case STQ:                                               // STQ:
            *(word*)(machine.mem + yy) = machine.Q;                        // Store rQ in yy
            machine.ip += 2;                                          // Advance to next word
            break;;
        case IMU:                                               // IMU:
            mul_tmp = machine.A * *(word*)(machine.mem + yy);   // Advance to next word
            machine.A = mul_tmp % 0x10000;                      // Store high word in Q
            machine.Q = floor(mul_tmp / 0x10000);               // Store  low word in A
            machine.ip += 2;                                          // Advance to next word
            break;;
        case IDV:                                               // IDV:
            machine.A = floor(machine.A / *(word*)(machine.mem + yy));     // Store Quotient  in A
            machine.Q = machine.A % *(word*)(machine.mem + yy);            // Store Remainder in Q
            machine.ip += 2;                                          // Advance to next word
            break;;
        case AZJ:                                               // AZJ:
            if (!machine.A)                                     // If rA == 0
                machine.ip = machine.mem + yy;                         //   Jump to yy
            else                                                // Otherwise
                machine.ip += 2;                                      //   Advance to next word
            break;;
        case AMJ:                                               // AMJ:
            if (machine.A < 0x00) {                              // If rA < 0
                machine.ip = machine.mem + yy;                         //   Jump to yy
            }
            else                                                // Otherwise
                machine.ip += 2;                                   //   Advance to next word
            break;;
        case NOP:                                               // NOP:
            machine.ip += 2;                                          // Advance to next word
            break;;
        case RDI:                                               // RDI:
            printf("%s", "Enter a number between 0 and 256: "); // Prompt for integer
            fgets(str_tmp,6,stdin);                             // Get integer
            sscanf(str_tmp,"%hu",(machine.mem + yy));           // Store input at yy
            machine.ip += 2;                                          // Advance to next word
            break;;
        case PRI:                                               // PRI:
            printf("%hd\n", *(machine.mem + yy));               // Print yy to stdout
            machine.ip += 2;                                          // Advance to next word
            break;;
        case UNJ:                                               // UNJ:
            machine.ip= machine.mem + yy;                          // Jump to yy
            break;;                                             
        case END:                                               // END:
            machine.ip = machine.mem + *(machine.mem + yy);        // Begin execution at yy
            break;; 
        default:
            printf("%s%lo: %x.\n", "Invalid operation at 0x", (machine.ip-machine.mem), op);
            return 1;
        }

    } while (machine.ip < (machine.mem+SZ) && op != HLT);
    // Report status
    //printf("Exited at location %lo with instruction %hx.\n", (machine.ip-machine.mem), *machine.ip >> 8);

    // If debug was requested, dump memory to file
    if (debug) dumpMem(SZ, machine);

    // Return assigned memory to the host machine
    free(machine.mem);

    return 0;

}

void 
dumpMem( const int SZ, RAMM machine){

    /* If machine is a valid RAMM virtual machine,
     * and SZ corresponds to the machine's memory size in words, then
     * dumpMem(machine,SZ) dumps the machine's memory to a file named
     * RAMM.dmp
     */

    // Open the dump file
    FILE* dmp = fopen("RAMM.dmp","wb");

    // Write raw memory to disk
    fwrite(machine.mem, sizeof(word),SZ,dmp);

}

void dumpRegs(RAMM machine){
   //printf("A: %hx\tQ: %hx\tIP: %hx\t Instruction: %hx,%hx\n", machine.A, machine.Q, machine.ip - machine.mem, *(machine.ip) >> 8, *(machine.ip) % 256);
   printf("A: %hx\tQ: %hx\tIP: %hx\t Instruction: %hx,%hx\n", machine.A, machine.Q, machine.ip - machine.mem, *(machine.ip + 1), *(machine.ip));
}

RAMM 
initializeMachine(const int SZ, const char* program){

    /* If program is a readable file containing a valid RAMM program,
     * initializeMachine(SZ,program) is a RAMM virtual machine whose
     * memory is SZ words and contains that program.
     */

    FILE* bin;
    char read;

    RAMM machine;
    machine.mem = (byte*)calloc(100,sizeof(word));
    machine.ip = machine.mem;


    // Pointer for traversing RAM
    //word* machine.ip = machine.mem; 

    // Read program into memory
    if ((bin = fopen(program,"rb")))
        fread(machine.mem,sizeof(word),100,bin);
    else
        printf("Could not open file %s\n",program);
    // Initialize registers to zero
    machine.A = machine.Q = 0;

    return machine;

}
